﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Veriface.Model.Common;

namespace Veriface.Model.Entity
{
    [AutoWire()]
    public class FaceModelContextFactory : IDesignTimeDbContextFactory<FaceModelContext>
    {
        public ServerSettings ServerSettings { get; set; }
        public FaceModelContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FaceModelContext>();
            optionsBuilder.UseSqlServer("Server=loanme.ceozeyz1dizv.ap-southeast-2.rds.amazonaws.com;Database=loanme_dev;User Id=sa;Password=Wanyy142!;");

            return new FaceModelContext(optionsBuilder.Options);
        }
        public FaceModelContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FaceModelContext>();
            optionsBuilder.UseSqlServer(ServerSettings.DbConnStr);

            return new FaceModelContext(optionsBuilder.Options);
        }
    }
    public class FaceModelContext : DbContext
    {
        public FaceModelContext(DbContextOptions<FaceModelContext> options)
            : base(options)
        { }
        public DbSet<User> Users { get; set; }
        public DbSet<Visitor> Visitors { get; set; }
        public DbSet<Face> Faces { get; set; }
        public DbSet<Client> Clients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Face>()
                .Property<string>("VectorsCollection")
                .HasField("_vectors");
        }
    }
}
