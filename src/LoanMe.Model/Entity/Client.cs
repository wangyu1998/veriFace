﻿using System;
using System.Collections.Generic;
using ServiceStack.DataAnnotations;
using Veriface.Model.Common;

namespace Veriface.Model.Entity {
    [Alias("Clients")]
    public class Client : IHasData<Guid> {
        [Required]
        [PrimaryKey]
        [Default(typeof(int), null)]
        public Guid Id { get; set; }

        [Default(typeof(string), null)]
        public string CustomName { get; set; }

        [Default(typeof(string), null)]
        public string Description { get; set; }

        [Default(typeof(string), null)]
        public string FirstCreatedBy { get; set; }

        [Default(typeof(string), null)]
        public string LastModifiedBy { get; set; }

        [Default(typeof(DateTime?), null)]
        public DateTime? FirstCreated { get; set; }

        [Default(typeof(DateTime?), null)]
        public DateTime? LastModified { get; set; }
        
        [Default(typeof(string), null)]
        public string CompanyAddress { get; set; }

        [Default(typeof(string), null)]
        public string CompanySuburb { get; set; }

        [Default(typeof(string), null)]
        public string CompanyState { get; set; }

        [Default(typeof(string), null)]
        public string CompanyPostCode { get; set; }

        [Default(typeof(string), null)]
        public string CompanyCity { get; set; }

        [Default(typeof(string), null)]
        public string CompanyCountry { get; set; }

        [Default(typeof(string), null)]
        public string PostalAddress { get; set; }

        [Default(typeof(string), null)]
        public string PostalSuburb { get; set; }

        [Default(typeof(string), null)]
        public string PostalState { get; set; }

        [Default(typeof(string), null)]
        public string PostalPostCode { get; set; }

        [Default(typeof(string), null)]
        public string PostalCity { get; set; }

        [Default(typeof(string), null)]
        public string PostalCountry { get; set; }

        [Default(typeof(string), null)]
        public string ContactPerson1 { get; set; }

        [Default(typeof(string), null)]
        public string ContactEmail1 { get; set; }

        [Default(typeof(string), null)]
        public string ContactPhone1 { get; set; }

        [Default(typeof(string), null)]
        public string ContactFax1 { get; set; }

        [Default(typeof(string), null)]
        public string ContactPerson2 { get; set; }

        [Default(typeof(string), null)]
        public string ContactEmail2 { get; set; }

        [Default(typeof(string), null)]
        public string ContactPhone2 { get; set; }

        [Default(typeof(string), null)]
        public string ContactFax2 { get; set; }

        [Reference]
        public List<User> Users { get; set; }
        [Reference]
        public List<Face> Faces { get; set; }
        [Reference]
        public List<Visitor> Visitors { get; set; }
    }
}