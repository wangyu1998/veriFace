﻿using System;
using ServiceStack.DataAnnotations;

namespace Veriface.Model.Entity
{
    public class Visitor
    {
        [Required]
        [PrimaryKey]
        public int Id { get; set; }

        [Default(typeof(string), null)]
        public Guid ClientId { get; set; }

        [Default(typeof(string), null)]
        public string GivenName { get; set; }

        [Default(typeof(string), null)]
        public string FamilyName { get; set; }

        [Default(typeof(string), null)]
        public string Phone { get; set; }

        [Default(typeof(string), null)]
        public string Email { get; set; }

        [Default(typeof(string), null)]
        public string Username { get; set; }


        [Default(typeof(string), null)]
        public string Password { get; set; }


        [Default(typeof(string), null)]
        public string Salt { get; set; }

        [Default(typeof(string), null)]
        public string Description { get; set; }

        [Default(typeof(string), null)]
        public string FirstCreatedBy { get; set; }

        [Default(typeof(string), null)]
        public string LastModifiedBy { get; set; }

        [Default(typeof(DateTime?), null)]
        public DateTime? FirstCreated { get; set; }

        [Default(typeof(DateTime?), null)]
        public DateTime? LastModified { get; set; }
    }
}
