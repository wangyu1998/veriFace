﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ServiceStack.DataAnnotations;
using Veriface.ServerCache;

namespace Veriface.Model.Entity
{
    public class Face
    {
        [Required]
        [PrimaryKey]
        public int Id { get; set; }

        [Default(typeof(string), null)]
        public Guid ClientId { get; set; }

        [Required]
        [Default(typeof(int), null)]
        public int VisitorId { get; set; }

        private string _vectors;

        [Required]
        [NotMapped]
        [Default(typeof(string), null)]
        public List<Double> Vectors {
            get => _vectors.Split(GlobalVars.Delimiter).Select(Double.Parse).ToList();
            set => _vectors = string.Join($"{GlobalVars.Delimiter}", value);
        }
    }
}
