﻿using Funq;
using ServiceStack.Configuration;
using ServiceStack.Data;

namespace Veriface.Model.Common
{
    public interface IServerSettings {
        string SeekUri { get; }
        string DbConnStr { get; }
    }

    [AutoWire(ReuseScope.Container,typeof(ServerSettings))]
    public class ServerSettings : IServerSettings {
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public MultiAppSettings AppSettings =>
            new MultiAppSettings(new OrmLiteAppSettings(DbConnectionFactory), new AppSettings());
        public string SeekUri => AppSettings.Get<string>("SeekUri");
        public string DbConnStr => AppSettings.Get<string>("DatabaseConnectionStr");
    }
}
