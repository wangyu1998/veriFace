﻿using System;
using System.Linq;
using System.Reflection;
using Funq;
using ServiceStack;
using ServiceStack.Logging;

namespace Veriface.Model.Common
{
    public class AutoWireAttribute : Attribute
    {
        public AutoWireAttribute(ReuseScope reuse = ReuseScope.None, Type wiredAs = null)
        {
            ReuseScope = reuse;
            WiredAs = wiredAs;
        }

        public ReuseScope ReuseScope { get; set; }
        public Type WiredAs { get; set; }

        public static void Register(Assembly assembly, Container container)
        {
            try {
                var types = assembly.GetTypes()
                    .Where(T => T.HasAttribute<AutoWireAttribute>());

                foreach (var type in types) {
                    var attr =
                        type.GetCustomAttributes(typeof(AutoWireAttribute), true).FirstOrDefault() as AutoWireAttribute;
                    var reuse = attr != null ? attr.ReuseScope : ReuseScope.None;
                    if (attr != null && attr.WiredAs != null) {
                        container.RegisterAutoWiredType(type, attr.WiredAs, reuse);
                    } else {
                        container.RegisterAutoWiredType(type, reuse);
                    }

                }
            } catch (ReflectionTypeLoadException ex) {
                var log = LogManager.GetLogger(typeof(AutoWireAttribute));

                log.Error("Failed to load components");
                foreach (Exception inner in ex.LoaderExceptions) {
                    log.Error(inner);
                }
                throw new Exception("Failed to load components (see log for details), first: " + (ex.LoaderExceptions.FirstOrDefault()?.Message ?? ""));
            }
        }

    }
}

