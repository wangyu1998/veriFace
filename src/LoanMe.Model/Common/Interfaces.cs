﻿using System;

namespace Veriface.Model.Common {
    public interface IHasData<TId> : IHasTypedId<TId>, IHasStampedUserId {
        string CustomName { get; set; }
        string Description { get; set; }
    }

    public interface IHasTypedId<TId> {
        TId Id { get; set; }
    }

    public interface IHasCreatedDate {
        DateTime? FirstCreated { get; set; }
    }

    public interface IHasStampedDate : IHasCreatedDate {
        DateTime? LastModified { get; set; }
    }

    public interface IHasCreatedUserId : IHasCreatedDate {
        string FirstCreatedBy { get; set; }
    }

    public interface IHasStampedUserId : IHasCreatedUserId, IHasStampedDate {
        string LastModifiedBy { get; set; }
    }
}