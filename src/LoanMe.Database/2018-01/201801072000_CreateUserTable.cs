﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoanMe.Database {
    public class CreateUserTable : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new {
                    UserId = table.Column<int>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true, maxLength: 80),
                    PasswordSalt = table.Column<string>(nullable: true, maxLength: 80),
                    LastDirectoryUpdate = table.Column<string>(nullable: true),
                    UserImage = table.Column<string>(nullable: true),
                    InsertDate = table.Column<string>(nullable: true),
                    InsertUserId = table.Column<int>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    UpdateUserId = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.UserId); });
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropTable(name: "Users");
        }
    }
}