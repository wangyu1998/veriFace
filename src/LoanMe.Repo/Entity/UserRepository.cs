﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack;
using Veriface.Model.Common;
using Veriface.Model.Entity;

namespace Veriface.Repo.Entity {
    [AutoWire]
    public class UserRepository: RepositoryBase {
        public User GetById(int userId) {
            throw new System.NotImplementedException();
        }
    }

    [AutoWire]
    public class FaceRepository : RepositoryBase {
        public FaceModelContextFactory FaceModelContextFactory { get; set; }
        public List<Face> GetByCustomerId(Guid customerId) {
            using (var context = FaceModelContextFactory.CreateDbContext()) {
                return context.Faces.Where(f => f.ClientId == customerId).ToList();
            }
        }

        public void Create(Face face) {
            using (var context = FaceModelContextFactory.CreateDbContext()) {
                context.Add(face);
                context.SaveChanges();
            }
        }
    }
    [AutoWire]
    public class VistitorRepository : RepositoryBase
    {
        public FaceModelContextFactory FaceModelContextFactory { get; set; }
        public Visitor GetById(int id)
        {
            using (var context = FaceModelContextFactory.CreateDbContext()) {
                return context.Visitors.FirstOrDefault(v => v.Id == id);
            }
        }
    }
    [AutoWire]
    public class ClientRepository : RepositoryBase
    {
        public FaceModelContextFactory FaceModelContextFactory { get; set; }
        public Client GetById(Guid id)
        {
            using (var context = FaceModelContextFactory.CreateDbContext()) {
                return context.Clients.FirstOrDefault(v => v.Id == id);
            }
        }
        public void Create(Client client)
        {
            using (var context = FaceModelContextFactory.CreateDbContext()) {
                context.Add(client);
                context.SaveChanges();
            }
        }
    }
}