﻿using ServiceStack;
using ServiceStack.Messaging;
using Veriface.Api.Async;

namespace Veriface.Api.Common {
    public class ServiceBase : Service {
        public string CustomerId { get; set; }
        public IMessageService MqService { get; set; }
        public void QueueAsyncRequest(ICustomerAsyncRequest req) {
            var envelope = new PostingEnvelope() {Request = req};
            using (var mqClient = MqService.CreateMessageQueueClient()) {
                mqClient.Publish(envelope);
            }

        }
    }
}