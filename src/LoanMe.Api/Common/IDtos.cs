﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using ServiceStack;
using ServiceStack.Web;
using Utilities;

namespace Veriface.Api.Common {
    #region "Common Basic Dto "

    public interface IHasRequestDto { }

    public interface IHasRestfulMethodCommandDto : IHasRequestDto {
        string Api { get; } //out read only
        string Method { get; set; } //GET POST PUT DELETE
        string Cmd { get; set; }
    }

    public interface IHasItemsDto<T> : IHasRestfulMethodCommandDto {
        IList<T> Items { get; set; }
    }

    public interface IHasIdDto<TId> : IHasRestfulMethodCommandDto {
        TId Id { get; set; }
    }

    public interface IHasCommonDataDto<T, TId> : IHasIdDto<TId>, IHasItemsDto<T> {
        //for validation    
    }

    public interface IHasParentDto<T> : IHasItemsDto<T> {
        string ContextId { get; set; } //the context's parent Id of this sub-data
    }

    public interface IHasPageSortDto {
        int? Page { get; set; }
        string Sort { get; set; }
    }

    #endregion

    #region "Response Dto"

    //response interfaces
    public interface IHasResponseDto : IHasResponseStatus { }

    public interface IHasResultResponseDto<T> {
        IList<T> Result { get; set; }
    }

    public interface IHasInfiniteResponseDto : IHasResponseDto {
        int? TotalCount { get; set; }

        int? PageSize { get; set; }
        //Put something like this in service when using page:
        //var limit = PreferenceManager.SystemConfig.Get("DataGridPageLimit").ToInteger();
        //var total = UserManager.Count(UserTab, where);
        //return UserManager.GetSorted(UserTab, where, dto.Sort).ToBasicDto(total, limit); //list
    }

    #endregion

    public static class DtoExtensions {
        #region " Dto Cmd Extensions "

        public static IList<string> GetActionAttributes(this IHasRestfulMethodCommandDto dto,
            ConcurrentDictionary<string, string> actions) {
            string attrs;
            actions.TryGetValue(dto.GetActionCmd(), out attrs);
            return (attrs == null) ? new List<string>() : attrs.ToLower().Split(',').Select(x => x.Trim()).ToList();
        }

        public static string GetActionCmd(this IHasRestfulMethodCommandDto dto) {
            return dto.Api + "," + (dto.Method) + "," + (dto.Cmd);
        }

        public static bool HasCmd(this IHasRestfulMethodCommandDto dto, string cmd) {
            if (dto.Cmd.IsNullOrWhiteSpace()) return false;
            return (dto.Cmd.ToLower() == cmd.ToLower());
        }

        public static bool CmdStartsWith(this IHasRestfulMethodCommandDto dto, string start) {
            if (dto.Cmd.IsNullOrWhiteSpace()) return false;
            return dto.Cmd.ToLower().StartsWith(start.ToLower());
        }

        public static bool CmdEndsWith(this IHasRestfulMethodCommandDto dto, string end) {
            if (dto.Cmd.IsNullOrWhiteSpace()) return false;
            return dto.Cmd.ToLower().EndsWith(end.ToLower());
        }

        public static bool HasMethod(this IHasRestfulMethodCommandDto dto, string method) {
            if (dto.Method.IsNullOrWhiteSpace()) return false;
            return dto.Method.ToLower() == method.ToLower();
        }

        public static bool HasMethod(this IHttpRequest req, string method) {
            if (req == null) return false;
            return req.HttpMethod.ToLower() == method.ToLower();
        }
        #endregion

       
    }
}