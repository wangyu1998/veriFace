﻿using System;
using ServiceStack;
using Veriface.Api.Common;
using Veriface.Model.Entity;
using Veriface.Repo.Entity;
using Veriface.ServerCache;

namespace Veriface.Api.Master
{
    [Route(_api)]
    public class SaveClientDto : IReturn<ClientDtoResponse>
    {
        public const string _api = GlobalVars.ApiBase + "/face/seek";
        public string Api => _api;
        public Client Client { get; set; }
    }
    [Route(_api)]
    public class GetClientDto : IReturn<ClientDtoResponse>
    {
        public const string _api = GlobalVars.ApiBase + "/face/seek";
        public string Api => _api;
        public Guid ClientId { get; set; }
    }
    public class ClientDtoResponse: IHasResponseDto
    {
        public Client Client { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

    [Authenticate]
    public class ClientService:ServiceBase
    {
        public ClientRepository ClientRepository { get; set; }
        public ClientDtoResponse Get(GetClientDto req) {
            return new ClientDtoResponse() {
                Client = ClientRepository.GetById(req.ClientId)
            };
        }
        public ClientDtoResponse Post(SaveClientDto req)
        {
            ClientRepository.Create(req.Client);
            return new ClientDtoResponse();
        }
    }
}
