﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceStack;
using Veriface.Api.Common;
using Veriface.Model.Entity;
using Veriface.Repo.Entity;
using Veriface.ServerCache;

namespace Veriface.Api.Master
{
    [Route(_api)]
    public class SeekDto : IReturn<SeekDtoResponse>
    {
        public const string _api = GlobalVars.ApiBase + "/face/seek";
        public string Api => _api;

        public IList<string> Images { get; set; }
    }
    public class SeekDtoResponse : IHasResponseDto
    {
        public Visitor Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

    [Route(_api)]
    public class FaceTrainDto : IReturn<FaceTrainDtoResponse>
    {
        public const string _api = GlobalVars.ApiBase + "/face/train";

        public string Api => _api;
        public string CustomerId { get; set; }
    }
    public class FaceTrainDtoResponse : IHasResponseDto
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    [Route(_api)]
    public class FaceSaveDto : IReturn<FaceTrainDtoResponse>
    {
        public const string _api = GlobalVars.ApiBase + "/face/save";

        public string Api => _api;
        public Guid CustomerId { get; set; }
        public IList<string> Images { get; set; }
    }
    public class FaceSaveDtoResponse : IHasResponseDto
    {
        public Face Face { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

    [Authenticate]
    public class FaceService : ServiceBase
    {
        public SeekApiClient SeekClient { get; set; }
        public FaceSvm FaceSvm { get; set; }
        public FaceRepository FaceRepository { get; set; }
        public VistitorRepository VistitorRepository { get; set; }
        public async Task<SeekDtoResponse> Get(SeekDto request)
        {
            var images = request.Images;
            var vector = await SeekClient.GetVactor(images);
            var visitorId = (int)FaceSvm.Decide(CustomerId, vector.ToArray());
            var visitor = VistitorRepository.GetById(visitorId);
            return new SeekDtoResponse() {
                Result = visitor
            };

        }
        public FaceTrainDtoResponse Post(FaceTrainDto request)
        {
            QueueAsyncRequest(new FaceTrainingAsyncRequest() { ClientId = Guid.Parse(request.CustomerId) });
            return new FaceTrainDtoResponse();
        }
        public async Task<FaceSaveDtoResponse> Post(FaceSaveDto request)
        {
            var images = request.Images;
            var vectors = await SeekClient.GetVactor(images);
            var face = new Face() {
                ClientId = request.CustomerId,
                Vectors = vectors
            };
            FaceRepository.Create(face);
            return new FaceSaveDtoResponse(){ Face = face};
        }
    }


    


}
