﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ServiceStack;
using ServiceStack.Auth;
using Veriface.Api.Common;
using Veriface.ServerCache;

namespace Veriface.Api.Master {
    [Route(_api)]
    public class AuthDto : IHasRestfulMethodCommandDto {
        public const string _api = GlobalVars.ApiBase + "/auth";

        public string Api {
            get { return _api; }
        }

        public string Cmd { get; set; } //validate login or swap or logout
        public string Username { get; set; } //validate can be null or validate name rules
        public string Password { get; set; } //validate can be null or validate password rules

        public string Method { get; set; }
    }

    [Route(_api)]
    public class RegisterDto : IHasRestfulMethodCommandDto {
        public const string _api = GlobalVars.ApiBase + "/register";

        public string Api {
            get { return _api; }
        }

        public string Cmd { get; set; } //validate login or swap or logout
        public string Username { get; set; } //validate can be null or validate name rules
        public string Password { get; set; } //validate can be null or validate password rules
        public string Email { get; set; }
        public string Method { get; set; }
    }

    public class CustomUserSession : AuthUserSession {
        [DataMember]
        public string CustomName { get; set; }

        [DataMember]
        public string CustomInfo { get; set; }

        public override void OnAuthenticated(IServiceBase authService, IAuthSession session,
            IAuthTokens tokens, Dictionary<string, string> authInfo) { }
    }


    public class AuthDtoResponse : IHasResponseDto {
        public AuthenticateResponse AuthResult { get; set; }
        public string LastLoggedIn { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public CustomUserSession Session { get; set; }
    }

    public class AuthService : Service {
        public AuthDtoResponse Get(AuthDto dto) {
            return new AuthDtoResponse() {
                Session = SessionAs<CustomUserSession>()
            };
        }

        public AuthDtoResponse Post(AuthDto dto) {
            using (var authService = ResolveService<AuthenticateService>()) {
                var response = authService.Authenticate(new Authenticate {
                    provider = CredentialsAuthProvider.Name,
                    UserName = dto.Username,
                    Password = dto.Password,
                    RememberMe = true,
                });
                return new AuthDtoResponse() {
                    AuthResult = response,
                    Session = SessionAs<CustomUserSession>()
                };
            }
        }

        public AuthDtoResponse Post(RegisterDto dto) {
            string hash;
            string salt;
            new SaltedHash().GetHashAndSaltString(dto.Password, out hash, out salt);

            var authRepo = (OrmLiteAuthRepository) TryResolve<IAuthRepository>();
            authRepo.CreateUserAuth(new UserAuth {
                DisplayName = dto.Username,
                Email = dto.Email,
                UserName = dto.Username,
                PasswordHash = hash,
                Salt = salt
            }, dto.Password);
            return new AuthDtoResponse();
        }
    }
}