﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack;
using Veriface.Model.Common;

namespace Veriface.Api.Master {
    public class SeekClientDto : IReturn<SeekClientResponse>
    {
        public IList<string> images { get; set; }
    }
    public class SeekClientResponse
    {
        public Double[] vector { get; set; }
    }
    [AutoWire]
    public class SeekApiClient
    {
        public ServerSettings ServerSettings { get; set; }

        public JsonServiceClient GetClient() {
            return new JsonServiceClient(ServerSettings.SeekUri);
        }
        public async Task<List<double>> GetVactor(IList<string> pic) {
            using (var client = GetClient()) {
                var response = await client.PostAsync<SeekClientResponse>("/vectorize/", new SeekClientDto() { images = pic });
                return response.vector.ToList();

            }
        }
        public List<double> GetVactorSync(IList<string> pic)
        {
            using (var client = GetClient()) {
                var response = client.Post<SeekClientResponse>("/vectorize/", new SeekClientDto() {images = pic});
                return response.vector.ToList();
            }
        }
    }
}