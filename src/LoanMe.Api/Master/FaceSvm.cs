﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using Funq;
using ServiceStack;
using ServiceStack.Caching;
using Veriface.Model.Common;
using Veriface.Model.Entity;
using Veriface.Repo.Entity;

namespace Veriface.Api.Master {
    [AutoWire(ReuseScope.Request)]
    public class FaceSvm
    {
        public FaceRepository FaceRepository { get; set; }
        public MulticlassSupportVectorMachine<Gaussian> Svm { get; set; }
        public ICacheClient CacheClient { get; set; }

        private string svmPrefix => "api::facesvm::svm::";

        public void Train(Guid customerId) {
            List<Face> faces = FaceRepository.GetByCustomerId(customerId);
            var inputs = faces.OrderBy(f => f.Id).Select(f => f.Vectors.ToArray()).ToArray();
            var outputs = faces.OrderBy(f => f.Id).Select(f => f.VisitorId).ToArray();
            var svm = new MulticlassSupportVectorLearning<Gaussian>() {
                Learner = (param) => new SequentialMinimalOptimization<Gaussian>() {
                    // Estimate a suitable guess for the Gaussian kernel's parameters.
                    // This estimate can serve as a starting point for a grid search.
                    UseKernelEstimation = true,
                    Kernel = Gaussian.FromGamma(0.001d)
                }
            };

            var machine = svm.Learn(inputs, outputs);
            using (var stream = new MemoryStream()) {
                Accord.IO.Serializer.Save(machine, stream);
                var bytes = stream.ToBytes();
                CacheClient.Set(svmPrefix + customerId, bytes);
            }

        }

        public void Load(string customerId) {
            var cache = CacheClient.Get<byte[]>(svmPrefix + customerId);
            using (var stream = new MemoryStream(cache)) {
                Svm = Accord.IO.Serializer.Load<MulticlassSupportVectorMachine<Gaussian>>(stream);
            }
        }


        public int Decide(string customerId,double[] v)
        {
            if (Svm == null) {
                Load(customerId);
            }

            return Svm.Decide(v);
        }
    }
}