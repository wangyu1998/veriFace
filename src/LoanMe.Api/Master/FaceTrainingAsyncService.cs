﻿using Veriface.Api.Async;
using Veriface.Model.Common;

namespace Veriface.Api.Master
{
    public class FaceTrainingAsyncRequest : CustomerAsyncRequest
    {

    }
    [AutoWire]
    public class FaceTrainingAsyncService : AsyncService<FaceTrainingAsyncRequest>
    {
        public FaceSvm FaceSvm { get; set; }
        public override object Execute(FaceTrainingAsyncRequest message) {
            FaceSvm.Train(message.ClientId);
            return true;
        }
    }
}
