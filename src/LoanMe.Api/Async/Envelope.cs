﻿using System;

namespace Veriface.Api.Async
{
    public class Envelope<T> where T : class
    {
        public T Message { get; set; }

        public Guid CustomerId { get; set; }
    }
    public class PostingEnvelope
    {
        public ICustomerAsyncRequest Request { get; set; }
    }
}
