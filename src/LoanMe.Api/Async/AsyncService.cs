﻿using System;
using ServiceStack.Data;

namespace Veriface.Api.Async
{
    public interface IAsyncService<T> where T : IAsyncRequest
    {
        Guid TrackingId { get; set; }
        object Execute(T message);
    }

    public abstract class AsyncService<TRequest> : IAsyncService<TRequest> where TRequest : IAsyncRequest
    {
        public IDbConnectionFactory DbFactory { get; set; }
        public Guid TrackingId { get; set; }

        public abstract object Execute(TRequest message);

        protected AsyncResponse RespondDone()
        {
            var response = new AsyncResponse();
            response.TrackingId = TrackingId;
            return response;
        }
    }
    public interface IAsyncRequest
    {
        Guid TrackingId { get; set; }
    }

    public interface ICustomerAsyncRequest : IAsyncRequest {
        Guid ClientId { get; set; }
        DateTime CreatedTime { get; set; }
    }

    public class CustomerAsyncRequest : ICustomerAsyncRequest
    {
        public CustomerAsyncRequest()
        {
            CreatedTime = DateTime.UtcNow;
        }
        public Guid ClientId { get; set; }
        public Guid TrackingId { get; set; }
        public DateTime CreatedTime { get; set; }
    }
    public class AsyncResponse
    {
        public AsyncResponse()
        {
            IsCompleted = true;
        }

        public Guid TrackingId { get; set; }
        public Guid? JournalId { get; set; }
        public Guid? RegardingId { get; set; }
        public bool IsCompleted { get; set; }

        public string Key {
            get { return GetKey(TrackingId); }
        }

        public static string GetKey(Guid trackingId)
        {
            return "asyncresponse:" + trackingId.ToString();
        }

        // Set to true for fire and forget items
        public bool IsResponseIgnored { get; set; }
    }
}
