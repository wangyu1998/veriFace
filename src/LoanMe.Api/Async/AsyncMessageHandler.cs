﻿using System;
using System.Collections.Generic;
using System.Linq;
using Funq;
using ServiceStack.Messaging;
using Veriface.Model.Common;

namespace Veriface.Api.Async
{
    [AutoWire()]
    public class AsyncMessageHandler
    {
        private static IEnumerable<Type> _apiAssemblyTypes;

        public Container Container { get; set; }

        public IMessageService MQService { get; set; }

        public ServerSettings ServerSettings { get; set; }

        public static object ResolveAsyncService(Type messageType, Container container)
        {
            _apiAssemblyTypes = _apiAssemblyTypes ?? typeof(AsyncService<>).Assembly.GetTypes();

            var baseServiceType = typeof(AsyncService<>);
            var serviceProtoType = baseServiceType.MakeGenericType(messageType);
            var asyncServiceTypes = _apiAssemblyTypes.Where(Ty => serviceProtoType.IsAssignableFrom(Ty)).ToArray();

            if (!asyncServiceTypes.Any())
                throw new InvalidOperationException(string.Format("No class implements CustomerAsyncService<{0}>", messageType.Name));

            if (asyncServiceTypes.Count() > 1)
                throw new InvalidOperationException(string.Format("Multiple classes implement CustomerAsyncService<{0}>", messageType.Name));

            var serviceType = asyncServiceTypes.Single();
            return container.Resolve(serviceType); 
        }


        // One queue for all financial postings
        public void RegisterPostingQueue()
        {
            MQService.RegisterHandler<PostingEnvelope>(M => {
                return ProcessPostingTransaction(M.Id, M.Body as PostingEnvelope);
            });
        }
        
        private bool ProcessPostingTransaction(Guid trackingId, PostingEnvelope envelope)
        {
            AsyncResponse response = null;

            try {
                var service = ResolveAsyncService(envelope.Request.GetType(), Container);
                var serviceType = service.GetType();
                var executeMethod = serviceType.GetMethod("Execute");
                response = (AsyncResponse)executeMethod.Invoke(service, new object[] { envelope.Request });

                return true;

            } catch (Exception ex) {

              
                return true;
            }

        }
        
    }
}
