﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Web;

#endregion

namespace Utilities {
    public enum CookiesType {
        Perm = 1,
        Temp = 0
    }

    public static partial class HttpExtensions {
        //get item from cookie or buffered items 

        //get header value
        public static string GetHeader(this IHttpRequest req, string name) {
            if (req == null || name.IsNullOrWhiteSpace()) return null;
            return req.Headers[name];
        }

        public static HashSet<string> GetHeaderHashSet(this IHttpRequest req, string name) {
            if (req == null || name.IsNullOrWhiteSpace()) return null;
            var str = req.Headers[name];
            return str == null ? new HashSet<string>() : new HashSet<string>(str.Split(','));
        }

        //set header value 
        public static void SetHeader(this IHttpResponse res, string name, string val) {
            if (res == null || name.IsNullOrWhiteSpace()) return;
            res.AddHeader(name, val);
        }

        public static void SetHeaderHashSet(this IHttpResponse res, string name, HashSet<string> hs) {
            if (res == null || name.IsNullOrWhiteSpace()) return;
            var str = hs == null ? "" : String.Join(",", hs.ToArray());
            res.AddHeader(name, str);
        }
    }
}