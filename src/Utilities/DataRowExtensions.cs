﻿using System.Data;

namespace Utilities {
    public static class DataRowExtensions {
        public static object Get(this DataRow data, string field) {
            if (data == null) return null;
            return data.IsNull(field) ? "" : data[field];
        }
    }
}