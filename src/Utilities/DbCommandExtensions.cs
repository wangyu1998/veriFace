﻿using System.Data.Common;

namespace Utilities {
    public static class DbCommandExtensions {
        public static DbCommand AddParameter(this DbCommand cmd, string name, object value) {
            var limitParam = cmd.CreateParameter();
            limitParam.ParameterName = name;
            limitParam.Value = value;
            cmd.Parameters.Add(limitParam);
            return cmd;
        }
    }
}