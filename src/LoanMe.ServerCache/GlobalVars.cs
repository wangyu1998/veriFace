﻿
namespace Veriface.ServerCache
{
    public static class GlobalVars {
        public const string ApiBase = "/api/v1";
        public static char Delimiter = ';';
    }
}