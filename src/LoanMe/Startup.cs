﻿using System.Net;
using Amazon;
using Funq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Aws.Sqs;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Mvc;
using ServiceStack.OrmLite;
using Veriface.Api.Async;
using Veriface.Api.Master;
using Veriface.Model;
using Veriface.Model.Common;
using Veriface.Model.Entity;
using Veriface.Repo.Entity;

namespace Veriface {
    public class Startup {
        public IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(
                    Configuration.GetConnectionString("DatabaseConnectionStr")
                    , SqlServerDialect.Provider));

            services.AddMvc();
            services.AddDbContext<FaceModelContext>(options => options.UseSqlServer(
                    Configuration.GetConnectionString("DatabaseConnectionStr"),
                    x => x.MigrationsAssembly("Veriface.Model")
                ));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            loggerFactory.AddConsole();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            DefaultFilesOptions options = new DefaultFilesOptions();
            options.DefaultFileNames.Clear();
            options.DefaultFileNames.Add("index.html");
            app.UseDefaultFiles(options);
            app.UseStaticFiles();
            app.UseServiceStack(new AppHost());
            
        }
    }

    public class AppHost : AppHostBase {
        public AppHost()
            : base("ServiceStack + .NET Core", typeof(AuthService).Assembly) { }

        public override void Configure(Funq.Container container) {
            Plugins.Add(new RazorFormat());

            //Works but recommend handling 404 at end of .NET Core pipeline
            //this.CustomErrorHttpHandlers[HttpStatusCode.NotFound] = new RazorHandler("/notfound");
            this.CustomErrorHttpHandlers[HttpStatusCode.Unauthorized] = new RazorHandler("/login");

            Plugins.Add(new AuthFeature(() => new CustomUserSession(),
                new IAuthProvider[] {
                    new CredentialsAuthProvider(), //HTML Form post of UserName/Password credentials
                    new BasicAuthProvider(), //Sign-in with HTTP Basic Auth
                    new DigestAuthProvider(AppSettings), //Sign-in with HTTP Digest Auth
                    new ApiKeyAuthProvider(AppSettings){
                        KeyTypes = new[] { "secret", "publishable" },
                        RequireSecureConnection = false
                    },
                    new CredentialsAuthProvider(AppSettings)
                }) {
                HtmlRedirect = "/",
                IncludeRegistrationService = true,
            });

            container.Register<IAuthRepository>(c =>
                new OrmLiteAuthRepository(c.Resolve<IDbConnectionFactory>()) {
                    UseDistinctRoleTables = AppSettings.Get("UseDistinctRoleTables", true),
                });
            container.Register<IMessageService>(c => new SqsMqServer(
                "AKIAJLXDNLEUWSMDQ74Q", "W46SxyTDGeBE0tQnfCQadCq6vNF3U2DtmXaDXvZ0", RegionEndpoint.APSoutheast2) {
                DisableBuffering = true, // Trade-off latency vs efficiency
            });
            container.Register(c =>
                new OrmLiteAppSettings(c.Resolve<IDbConnectionFactory>()));
            //Create the ConfigSettings table if it doesn't exist
            container.Resolve<OrmLiteAppSettings>().InitSchema();
            RegisterService(container);
            RegisterMqHandler(container);
        }

        void RegisterMqHandler(Container container) {
            var mqServer = container.Resolve<IMessageService>();
            container.Resolve<AsyncMessageHandler>().RegisterPostingQueue();
            mqServer.Start();
        }

        void RegisterService(Container container) {
            container.Register<ICacheClient>(new MemoryCacheClient());
            
            AutoWireAttribute.Register(typeof(AuthService).Assembly, container);  // Api
            AutoWireAttribute.Register(typeof(FaceRepository).Assembly, container);  // repo
            AutoWireAttribute.Register(typeof(FaceModelContext).Assembly, container);  // Model
        }
    }
}