import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class Repo {

    static $inject = ['$http'];
    public api:string = '/api/v1';
    constructor(private http:Http){
    }
    
    public register(username:string, password:string, email:string) {        
        return this.http.post(this.api+'/register',{username:username,password:password,email:email});
    }
    

    public login(username:string, password:string) {        
        return this.http.post(this.api+'/auth',{username:username,password:password})
        .map((res) => {
            if (res) {
                localStorage.setItem('auth_token',JSON.stringify(res));
            }
            return res;
        });
    }
    
    logout() {
        localStorage.removeItem('auth_token');
    }

    isLoggedIn() {
        return !!localStorage.getItem('auth_token');;
    }

}
