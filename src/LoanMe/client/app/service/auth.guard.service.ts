import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { Repo } from './Repo'

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    private loggedIn = false;
    constructor(private router: Router) { }
    canActivate() {
        if (!!localStorage.getItem('auth_token')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['./login']);
        return false;
    }

    canActivateChild() {
        return !!localStorage.getItem('auth_token');;
    }

}

@Injectable()
export class UnauthGuard implements CanActivate, CanActivateChild {

    private loggedIn = false;
    constructor(private router: Router) { }
    canActivate() {
        if (!!localStorage.getItem('auth_token')) {
            // logged in so return true
            this.router.navigate(['./dashboard']);
            return true;
        }

        // not logged in so redirect to login page with the return url
        return false;
    }

    canActivateChild() {
        return !!localStorage.getItem('auth_token');;
    }

}