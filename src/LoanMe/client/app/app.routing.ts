import { Routes, RouterModule } from '@angular/router';
import { Pages } from './pages';
import {UnauthGuard, AuthGuard } from './service/auth.guard.service';


export const routes: Routes = [
  { path: 'register', loadChildren: () => System.import('./register/register.module') },
  { path: '', redirectTo: 'pages/dashboard', pathMatch: 'full', canActivate:[AuthGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
  
];

export const routing = RouterModule.forRoot(routes, { useHash: true });
